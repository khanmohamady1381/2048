import java.util.*;
public class Main {

    public static void print_First_Line(){

        for (int i = 0; i < 18; i++) {//print(_)

            if ((i == 0) || (i == 5) || (i == 9) || (i == 13) || (i == 17)) {
                if (i == 0) {

                    System.out.print("\u250C");
                    continue;

                }
                if (i == 17) {
                    System.out.print("\u2510");
                    continue;
                }
                System.out.print("\u252c");

            }

            System.out.print("\u2500");
        }

    }

    public static void print_Vertical_Lines(int [][]board,int i){

            for (int j = 0 ; j < 4 ; j++){// will check is that home of board 0 or not
                
                System.out.print("\u2502");
                
                if (board[i][j]!=0){//if the home was not 0 we should count the number of digits 
                                    // and then put it in the correct home for printing 
                    int length = (int) (Math.log10(board[i][j]) + 1);//count digits

                    if (length==1){//1 digits need 1 space before and 2 spaces after
                        System.out.print(" "+board[i][j]+"  ");
                    }else
                        if (length==2){//2 digits need 1 spaces before and after
                            System.out.print(" "+board[i][j]+" ");
                        }else
                            if (length==3){//3 digits need 1 space before 
                                System.out.print(" "+board[i][j]);
                            }else
                                if (length==4){//4 digits dont need any space
                                    System.out.print(board[i][j]);
                                }

                }
                else//if that was not number will print 4 space 
                                {
                                    System.out.print("    ");
                                }
            }
            System.out.print("\u2502");
        }
    


    public static void print_Middle_Straight_Lines(){

        for (int  i = 0 ; i < 5 ; i++){

            if(i==0){

                System.out.print("\u251C");
                for (int j = 0 ; j < 4 ; j++){
                    System.out.print("\u2500");
                }
                continue;


            }

            if (i==4){

                System.out.print("\u2524"+"    ");

                continue;
            }
            System.out.print("\u253C");
            for (int j = 0 ; j < 4 ; j++){
                System.out.print("\u2500");
            }

        }

    }

    

    public static void print_Last_Line(){

        
        for (int i = 0 ; i < 5 ; i++){

            if (i==0){

                System.out.print("\u2514");
                
                for (int j = 0 ; j < 4 ; j++){
                 
                    System.out.print("\u2500");
                
                }
                
                continue;

            }

            if (i==4){

                System.out.print("\u2518");

                continue;
            }

            System.out.print("\u2534");

            for (int j = 0 ; j < 4 ; j++){
                System.out.print("\u2500");
            }


        }

    }


    public static void Print(int [][]board) {

        print_First_Line();

        System.out.println();

        print_Vertical_Lines(board,0);

        System.out.println();



        print_Middle_Straight_Lines();

        System.out.println();

        print_Vertical_Lines(board,1);

        System.out.println();



        print_Middle_Straight_Lines();

        System.out.println();

        print_Vertical_Lines(board,2);

        System.out.println();



        print_Middle_Straight_Lines();

        System.out.println();

        print_Vertical_Lines(board,3);

        System.out.println();

        print_Last_Line();


    }


    

    public static void make_Zero(int [][] board){

        for (int[] i : board){
            for (int j : i) {
                j = 0;
            }

        }

    }

    public static void random_set(int [][]board){//will fill a empty home of board
                                                 //to fill it by 2 or 4
        int i;
        int j;
        do {//finding random home of board to
            //check is that 0 or not
            Random rand = new Random();
            i = rand.nextInt(4);
            j = rand.nextInt(4);

        }while (board[i][j]!=0);

        Random rand = new Random();
        int chance = rand.nextInt(10);//chance of filling empty home
                                        //by 2 is 90% and for 4 is 10%
        

        
        if (chance==0){

            board[i][j]=4;

        }else{

            board[i][j]=2;

        }
    

    }

    public static boolean is_Full (int [][] board){//will check is all home of 
                                                   //board full of numbers
        for (int i = 0 ; i < 4 ; i++){
            for (int j = 0 ; j < 4; j++ ){
                if(board[i][j]==0){
                    return true;
                }
            }
        }
        return false;
    }


    public static void check_Move(String move , int [][]board){//by using keyboard will change the 
                                                               //home of numbers

        switch (move){

            case "w":

                for (int i =0 ; i < 4 ; i++){      //check 4 row

                    int temp = 0 ;

                    for (int j = 0 ; j < 4 ; j++){//check every home of row

                        if (board[j][i]!=0){       //if that home was not 0 change with front of that

                             int temp1 = board[temp][i];
                             board[temp][i]=board[j][i];
                             board[j][i]=temp1;
                             temp++;

                        }

                    }

                }

                break;

            case "s":

                for (int i =0 ; i < 4 ; i++){      //check 4 row

                    int temp = 3 ;

                    for (int j = 3 ; j >= 0 ; j--){//check every home of row

                        if (board[j][i]!=0){       //if that home was not 0 change with front of that

                            int temp1 = board[temp][i];
                            board[temp][i]=board[j][i];
                            board[j][i]=temp1;
                            temp--;

                        }

                    }

                }

                break;

            case "a":

                for (int i =0 ; i < 4 ; i++){      //check 4 row

                    int temp = 0 ;

                    for (int j = 0 ; j < 4 ; j++){//check every home of row

                        if (board[i][j]!=0){       //if that home was not 0 change with front of that

                            int temp1 = board[i][temp];
                            board[i][temp]=board[i][j];
                            board[i][j]=temp1;
                            temp++;

                        }

                    }

                }

                break;

            case "d":

                for (int i =0 ; i < 4 ; i++){      //check 4 row

                    int temp = 3 ;

                    for (int j = 3 ; j >= 0 ; j--){//check every home of row

                        if (board[i][j]!=0){       //if that home was not 0 change with front of that

                            int temp1 = board[i][temp];
                            board[i][temp]=board[i][j];
                            board[i][j]=temp1;
                            temp--;

                        }

                    }


                }

                break;

        }


    }


    public static void sum_home_board( int[][] board,char move){
   
    if(move=='w'){// if user use w in game this will sum 2 board 
                  // that are together that have same number
        
        for (int i = 0 ; i < 4; i++ ){
            for (int j = 0; j < 3; j++ ){//if j<4 so we will have bug in board[j+1][i]
                
                if (board[j][i]==board[j+1][i]){
                    
                    board[j][i]+=board[j][i];
                    board[j+1][i]=1; //we will make sign by using number 1 to avoid 
                                     //summing 4 home of boards that have same number
                    
                                     
                }

                for(int m = 0; m < 4 ; m++){//we will use that sign that we made by number 1
                   if(board[j+1][m]==1){
         
                    board[j+1][m]=0;
         
                   }
               
                }

            }
   
        }

    }

    if(move=='s'){
        for (int i = 0 ; i < 4; i++ ){
            for (int j = 2; j >= 0 ; j--){
                if (board[j][i]==board[j+1][i]){
                    
                    board[j+1][i]+=board[j+1][i];
                    board[j][i]=1;   //we will make sign by using number 1 to avoid 
                                     //summing 4 home of boards that have same number

                }

                for(int m = 0; m < 4 ; m++){//we will use that sign that we made by number 1
                    if(board[j][m]==1){

                        board[j][m]=0;

                    }

                }

            }

        }

    }

     if(move=='a'){

         for (int i = 0 ; i < 4 ; i++ ){
                for (int j = 0; j < 3; j++ ){
                    if (board[i][j]==board[i][j+1]){
                        
                        board[i][j]+=board[i][j];
                        board[i][j+1]=1;    //we will make sign by using number 1 to avoid 
                                            //summing 4 home of boards that have same number

                    }

                    for(int m = 0; m < 4 ; m++){//we will use that sign that we made by number 1
                        if(board[i][m]==1){

                            board[i][m]=0;

                        }

                    }

                }

            }

        }

    if(move=='d'){
        for (int i = 0 ; i < 4 ; i++ ){
            for (int j = 2; j >= 0 ; j--){
                if (board[i][j]==board[i][j+1]){
                   
                    board[i][j+1]+=board[i][j+1];
                    board[i][j]=1;  //we will make sign by using number 1 to avoid 
                                    //summing 4 home of boards that have same number
                    
                }

                for(int m = 0; m < 4 ; m++){//we will use that sign that we made by number 1
                    if(board[i][m]==1){

                        board[i][m]=0;

                    }

                }

            }

        }

    }

}


public static boolean is_Finished(int [][]board){//will check that is player lose

    for(int i=0; i < 4 ; i++){//if board has any empty will return false
        for(int j=0; j < 4; j++){

            if(board[i][j]==0){
                
                return false;
            }
            
        }
    }


        for(int i=0; i < 4 ; i++){   //if board has any two numbers that  
                                     //is beside each other in row will retune false
            for(int j=0; j < 3; j++){
             
                if(board[i][j]==board[i][j+1]){
                    return false;
                }
                
            }
        }
            for( int i=0; i < 3 ; i++){  //if board has any two numbers that  
                                         //is beside each other in column will retune false
                for(int j=0; j < 4; j++){
                 
                    if(board[i][j]==board[i+1][j]){
                        return false;
                    }
                    
                }

            }
            

return true ;
}



    public static void main(String[] args)  {

        Scanner cin = new Scanner(System.in);

        final int size = 4 ;

        int [][] board = new int[size][size];
        make_Zero(board);

        random_set(board);
        random_set(board);

        String move;

        Print(board);

        while(!is_Finished(board)){
            
            move = cin.next();
            char z = move.charAt(0);

            check_Move(move,board);
            sum_home_board(board,z);
            check_Move(move,board);
       
            if(is_Full(board)){
               
                random_set(board);
            
            }
            
             Print(board);
        }

        System.out.println("Sorry,you lose!!! ");
        




    }
}
